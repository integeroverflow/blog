---
title: About Me
date: 2010-09-20 15:22:28
tags:
---

## Few Lines About Me

I don\'t like to talk about myself because I really don\'t know <pre class=\"brush: perl; gutter: true; first-line: 1; highlight: []; html-script: false\">who am i;</pre> But let me talk about what I do, What I Like and what I dislike. So lets get started:\r\n\r\nI like Technology, I like gadgets, I like programming languages, I like opensource, I like Google and every service they offer, I like JavaScript I like jQuery, I like angularJS, I like React, I like Electron, I like Meteor, I like stackoverflow, I like Music. I Like David Guetta, Marc Anthony, Enrique Iglesias. I Love Romanian music. I like Akcent, Edward Maya. About Indian Music, don\'t even ask, I have got them covered from M. Rafi to Arjit, I listen to all of those guys selected songs. I Like exceptionally good food. I am also a movie buff.\r\n\r\nI love to help people on <a href=\"http://stackoverflow.com/users/724764/defau1t\" target=\"_blank\">Stack overflow</a> if I can. I also love open source. Currently I am working on Dexter Blog Engine. \r\n\r\nI love creativity, art, typography, innovation and user experience. I also like Current Affairs, British Accent, Sarcasm and humor. I Love twitter though I don\'t tweet much, rather I re-tweet what interests me . I really hate Facebook. I prefer communicating over phone so call or whatsApp me on 09035790930. I even love reading emails. so you should email me at singularity.javid@gmail.com\r\n', 'About', '', 'inherit', 'closed', 'closed', '', '158-revision-v1', '', '', '2016-12-24 22:27:46', '2016-12-24 16:57:46', '', 158, 'http://javidrashid.com/158-revision-v1/', 0, 'revision', '', 0);
