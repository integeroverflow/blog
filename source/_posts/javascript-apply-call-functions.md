---
title: JavaScript Apply and Call functions Usage
date: 2011-09-20 15:22:28
tags:
---
'JavaScript call and apply are functions that are used to call function within the context of another function. There is a fundamental difference between the two, call() takes parameters as a name while as apply() accepts parameters as an array.\n\nI will show how to use call and apply to change the background of a page in JavaScript. Usually you won\'t do such a basic thing as changing background via call or apply methods, but I wont to touch the basics of apply and call and show how to use them in function context. Otherwise you would never want to change the page background or for that matter any style property with JavaScript call and apply methods.\n\nJavaScript call() function with Example.\n\n



``` bash
$ function changeBackgroundColor(color) { this.style.backgroundColor = color;		this.innerHTML = &quot;&lt;h1&gt;Body Background Changed Via JavaScript Call Function&lt;/h1&gt;&quot;; var bodyBackground = document.getElementsByTagName(&quot;body&quot;)[0]; changeBackgroundColor.call(bodyBackground,&quot;#990000&quot;);
```



Quite simple, we use a function changeBackgroundColor(), pass it a parameter color. In this function we set the bodybackground with the follow statement\nthis.style.backgroundColor = color;\nwe also set the innerHTML of body to be a h1 tag with some descriptive text.\n\nNow to call changeBackgroundColor function we use call like  changeBackgroundColor.call(bodyBackground,\"#990000\");\n\nAs I said call takes parameters as name, the bodyBackground is the node that we want the background to be changed. In our case body tag, the we pass the color that page background should take.\n\nBelow is the Demo of the above example in jsFiddle:\n\n
``` bash
<iframe src=\"http://jsfiddle.net/refhat/Z4Fhg/\" height=\"200\" width=\"300\">http://jsfiddle.net/refhat/Z4Fhg/</iframe>', 'JavaScript call and apply with Examples', '', 'inherit', 'open', 'open', '', '396-revision-v1', '', '', '2013-12-22 15:41:47', '2013-12-22 10:11:47', '', 396, 'http://javidrashid.com/blog/396-revision-v1/', 0, 'revision', '', 0),
```